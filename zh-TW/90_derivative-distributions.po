# AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2020-05-17 17:54+0200\n"
"PO-Revision-Date: 2019-01-27 14:56+0000\n"
"Last-Translator: Louies <louies0623@gmail.com>\n"
"Language-Team: Chinese (Traditional) <https://hosted.weblate.org/projects/debian-handbook/90_derivative-distributions/zh_Hant/>\n"
"Language: zh-TW\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.5-dev\n"

#, fuzzy
#| msgid "Live CD"
msgid "live CD"
msgstr "Live CD"

msgid "Specificities"
msgstr "特殊性"

msgid "Particular Choices"
msgstr "特殊選擇"

msgid "Derivative Distributions"
msgstr "衍生產品"

msgid "<indexterm><primary>derivative distribution</primary></indexterm> <indexterm><primary>distribution, derivative</primary></indexterm> Many Linux distributions are derivatives of Debian and reuse Debian's package management tools. They all have their own interesting properties, and it is possible one of them will fulfill your needs better than Debian itself."
msgstr ""

msgid "Census and Cooperation"
msgstr "調查與合作"

msgid "The Debian project fully acknowledges the importance of derivative distributions and actively supports collaboration between all involved parties. This usually involves merging back the improvements initially developed by derivative distributions so that everyone can benefit and long-term maintenance work is reduced."
msgstr "Debian 專案充分認識到衍生產品分配的重要性，並積極支援所有相關方之間的合作。這通常涉及合併最初由衍生資料分配開發的改進，以便每個人都能受，並減少長期維護工作。"

msgid "This explains why derivative distributions are invited to become involved in discussions on the <literal>debian-derivatives@lists.debian.org</literal> mailing-list, and to participate in the derivative census. This census aims at collecting information on work happening in a derivative so that official Debian maintainers can better track the state of their package in Debian variants. <ulink type=\"block\" url=\"https://wiki.debian.org/DerivativesFrontDesk\" /><ulink type=\"block\" url=\"https://wiki.debian.org/Derivatives/Census\" />"
msgstr ""

msgid "Let us now briefly describe the most interesting and popular derivative distributions."
msgstr "現在讓我們簡單描述一下最有趣和最流行的分支分佈。"

msgid "Ubuntu"
msgstr "Ubuntu"

msgid "<primary>Ubuntu</primary>"
msgstr "<primary>Ubuntu</primary>"

#, fuzzy
#| msgid "Ubuntu made quite a splash when it came on the Free Software scene, and for good reason: Canonical Ltd., the company that created this distribution, started by hiring thirty-odd Debian developers and publicly stating the far-reaching objective of providing a distribution for the general public with a new release twice a year. They also committed to maintaining each version for a year and a half."
msgid "Ubuntu made quite a splash when it came on the free software scene, and for good reason: Canonical Ltd., the company that created this distribution, started by hiring thirty-odd Debian developers and publicly stating the far-reaching objective of providing a distribution for the general public with a new release twice a year. They also committed to maintaining each version for a year and a half."
msgstr "當 Ubuntu 出現在免費軟件領域時，它引起了很大的轟動，並且有充分的理由：Canonical這家發行版本公司，開始雇傭了30多個 Debian 開發者，並公開聲明了一個深遠的目標，即每年向公眾提供兩次新的發行版本。他們還承諾將每個版本將會維護一年半。"

#, fuzzy
#| msgid "These objectives necessarily involve a reduction in scope; Ubuntu focuses on a smaller number of packages than Debian, and relies primarily on the GNOME desktop (although an official Ubuntu derivative, called “Kubuntu”, relies on KDE). Everything is internationalized and made available in a great many languages."
msgid "These objectives necessarily involve a reduction in scope; Ubuntu focuses on a smaller number of packages than Debian, and relies primarily on the GNOME desktop (although there are Ubuntu derivatives that come with other desktop environments). Everything is internationalized and made available in a great many languages."
msgstr "這些目標必然涉及範圍的縮小；Ubuntu 關注的套裝軟體數量比 Debian 少，並且主要依賴於 Gnome 桌面（儘管官方的 Ubuntu 衍生產品 “kubuntu” 依賴於KDE）。一切都是國際化的，並以多種語言提供。"

msgid "<primary>Kubuntu</primary>"
msgstr "<primary>Kubuntu</primary>"

msgid "So far, Ubuntu has managed to keep this release rhythm. They also publish <emphasis>Long Term Support</emphasis> (LTS) releases, with a 5-year maintenance promise. As of June 2019, the current LTS version is version 18.04, nicknamed Bionic Beaver. The last non-LTS version is version 19.04, nicknamed Disco Dingo. Version numbers describe the release date: 19.04, for example, was released in April 2019."
msgstr ""

msgid "<emphasis>IN PRACTICE</emphasis> Ubuntu's support and maintenance promise"
msgstr ""

msgid "<primary><literal>main</literal></primary>"
msgstr "<primary><literal>main</literal></primary>"

msgid "<primary><literal>restricted</literal></primary>"
msgstr "<primary><literal>restricted</literal></primary>"

msgid "<primary><literal>universe</literal></primary>"
msgstr "<primary><literal>宇宙</literal></primary>"

msgid "<primary><literal>multiverse</literal></primary>"
msgstr "<primary><literal>多重宇宙</literal></primary>"

msgid "Canonical has adjusted multiple times the rules governing the length of the period during which a given release is maintained. Canonical, as a company, promises to provide security updates to all the software available in the <literal>main</literal> and <literal>restricted</literal> sections of the Ubuntu archive, for 5 years for LTS releases and for 9 months for non-LTS releases. Everything else (available in the <literal>universe</literal> and <literal>multiverse</literal>) is maintained on a best-effort basis by volunteers of the MOTU team (<emphasis>Masters Of The Universe</emphasis>). Be prepared to handle security support yourself if you rely on packages of the latter sections."
msgstr ""

msgid "Ubuntu has reached a wide audience in the general public. Millions of users were impressed by its ease of installation, and the work that went into making the desktop simpler to use."
msgstr "Ubuntu 已經在公眾中廣受歡迎。數以百萬計的用戶對它的易於安裝和使桌面更易於使用的工作印象深刻。"

msgid "Ubuntu and Debian used to have a tense relationship; Debian developers who had placed great hopes in Ubuntu contributing directly to Debian were disappointed by the difference between the Canonical marketing, which implied Ubuntu were good citizens in the Free Software world, and the actual practice where they simply made public the changes they applied to Debian packages. Things have been getting better over the years, and Ubuntu has now made it general practice to forward patches to the most appropriate place (although this only applies to external software they package and not to the Ubuntu-specific software such as Mir or Unity). <ulink type=\"block\" url=\"https://www.ubuntu.com/\" />"
msgstr ""

msgid "Linux Mint"
msgstr "Linux Mint"

msgid "<primary>Linux Mint</primary>"
msgstr "<primary>Linux Mint</primary>"

#, fuzzy
#| msgid "Linux Mint is a (partly) community-maintained distribution, supported by donations and advertisements. Their flagship product is based on Ubuntu, but they also provide a “Linux Mint Debian Edition” variant that evolves continuously (as it is based on Debian Testing). In both cases, the initial installation involves booting a LiveDVD."
msgid "Linux Mint is a (partly) community-maintained distribution, supported by donations and advertisements. Their flagship product is based on Ubuntu, but they also provide a “Linux Mint Debian Edition” variant that evolves continuously (as it is based on Debian Testing). In both cases, the initial installation involves booting a live DVD or a live USB storage device."
msgstr "LinuxMint 是一個（部分）由社區維護的發行版本，由捐贈和廣告支持。他們的旗艦產品是基於 Ubuntu 的，但是他們也提供了一個 “LinuxMint Debian版本” 的變體，這種變體不斷發展（因為它是基於 Debian 測試）。在這兩種情况下，初始安裝都需要啟動 LiveDVD。"

#, fuzzy
#| msgid "The distribution aims at simplifying access to advanced technologies, and provides specific graphical user interfaces on top of the usual software. For instance, Linux Mint relies on Cinnamon instead of GNOME by default (but it also includes MATE as well as KDE and Xfce); similarly, the package management interface, although based on APT, provides a specific interface with an evaluation of the risk from each package update."
msgid "The distribution aims at simplifying access to advanced technologies, and provides specific graphical user interfaces on top of the usual software. For instance, Linux Mint relies on Cinnamon instead of GNOME by default (but it also includes MATE as well as Xfce); similarly, the package management interface, although based on APT, provides a specific interface with an evaluation of the risk from each package update."
msgstr "發行版本旨在簡化對高級科技的存取，並在常規軟體的基礎上提供特定的圖形使用者介面。例如:Linux Mint 預設依賴肉桂而不是 Gnome（但它還包括 MATE 以及 KDE 和 Xfce）；同樣，封包管理介面，儘管基於APT，但提供了一個特定的介面，用於評估每個封包更新的風險。"

msgid "Linux Mint includes a large amount of proprietary software to improve the experience of users who might need those. For example: Adobe Flash and multimedia codecs. <ulink type=\"block\" url=\"https://linuxmint.com/\" />"
msgstr ""

msgid "Knoppix"
msgstr "Knoppix"

#, fuzzy
#| msgid "<primary><foreignphrase>LiveCD</foreignphrase></primary>"
msgid "<primary><foreignphrase>live CD</foreignphrase></primary>"
msgstr "<primary><foreignphrase>LiveCD</foreignphrase></primary>"

msgid "<primary>Knoppix</primary>"
msgstr "<primary>Knoppix</primary>"

msgid "<primary>bootable CD-ROM</primary>"
msgstr "<primary>bootable CD-ROM</primary>"

msgid "<primary>CD-ROM</primary><secondary>bootable</secondary>"
msgstr "<primary>CD-ROM</primary><secondary>bootable</secondary>"

msgid "The Knoppix distribution barely needs an introduction. It was the first popular distribution to provide a <emphasis>live CD</emphasis>; in other words, a bootable CD-ROM that runs a turn-key Linux system with no requirement for a hard-disk — any system already installed on the machine will be left untouched. Automatic detection of available devices allows this distribution to work in most hardware configurations. The CD-ROM includes almost 2 GB of (compressed) software, and the DVD-ROM version has even more."
msgstr ""

msgid "Combining this CD-ROM to a USB stick allows carrying your files with you, and to work on any computer without leaving a trace — remember that the distribution doesn't use the hard-disk at all. Knoppix uses LXDE (a lightweight graphical desktop) by default, but the DVD version also includes GNOME and Plasma. Many other distributions provide other combinations of desktops and software. This is, in part, made possible thanks to the <emphasis role=\"pkg\">live-build</emphasis> Debian package that makes it relatively easy to create a live CD. <ulink type=\"block\" url=\"https://live-team.pages.debian.net/live-manual/\" />"
msgstr ""

msgid "<primary><emphasis role=\"pkg\">live-build</emphasis></primary>"
msgstr "<primary><emphasis role=\"pkg\">live-build</emphasis></primary>"

msgid "Note that Knoppix also provides an installer: you can first try the distribution as a live CD, then install it on a hard-disk to get better performance. <ulink type=\"block\" url=\"https://www.knopper.net/knoppix/index-en.html\" />"
msgstr ""

msgid "Aptosid and Siduction"
msgstr "Aptosid 和 Siduction"

msgid "<primary>Sidux</primary>"
msgstr "<primary>Sidux</primary>"

msgid "<primary>Aptosid</primary>"
msgstr "<primary>Aptosid</primary>"

msgid "<primary>Siduction</primary>"
msgstr "<primary>Siduction</primary>"

msgid "These community-based distributions track the changes in Debian <emphasis role=\"distribution\">Sid</emphasis> (<emphasis role=\"distribution\">Unstable</emphasis>) — hence their name. The modifications are limited in scope: the goal is to provide the most recent software and to update drivers for the most recent hardware, while still allowing users to switch back to the official Debian distribution at any time. Aptosid was previously known as Sidux, and Siduction is a more recent fork of Aptosid. <ulink type=\"block\" url=\"http://aptosid.com\" /> <ulink type=\"block\" url=\"https://siduction.org\" />"
msgstr ""

msgid "Grml"
msgstr "Grml"

msgid "<primary>Grml</primary>"
msgstr "<primary>Grml</primary>"

msgid "Grml is a live CD with many tools for system administrators, dealing with installation, deployment, and system rescue. The live CD is provided in two flavors, <literal>full</literal> and <literal>small</literal>, both available for 32-bit and 64-bit PCs. Obviously, the two flavors differ by the amount of software included and by the resulting size. <ulink type=\"block\" url=\"https://grml.org\" />"
msgstr ""

msgid "Tails"
msgstr "Tails"

msgid "<primary>Tails</primary>"
msgstr "<primary>Tails</primary>"

msgid "Tails (The Amnesic Incognito Live System) aims at providing a live system that preserves anonymity and privacy. It takes great care in not leaving any trace on the computer it runs on, and uses the Tor network to connect to the Internet in the most anonymous way possible. <ulink type=\"block\" url=\"https://tails.boum.org\" />"
msgstr ""

msgid "Kali Linux"
msgstr "Kali Linux"

msgid "<primary>Kali</primary>"
msgstr "<primary>Kali</primary>"

msgid "<primary>penetration testing</primary>"
msgstr "<primary>滲透測試</primary>"

msgid "<primary>forensics</primary>"
msgstr "<primary>取得證明</primary>"

msgid "Kali Linux is a Debian-based distribution specializing in penetration testing (“pentesting” for short). It provides software that helps auditing the security of an existing network or computer while it is live, and analyze it after an attack (which is known as “computer forensics”). <ulink type=\"block\" url=\"https://kali.org\" />"
msgstr ""

msgid "Devuan"
msgstr "Devuan"

msgid "<primary>Devuan</primary>"
msgstr "<primary>Devuan</primary>"

msgid "Devuan is a fork of Debian started in 2014 as a reaction to the decision made by Debian to switch to <command>systemd</command> as the default init system. A group of users attached to <command>sysv</command> and opposing drawbacks to <command>systemd</command> started Devuan with the objective of maintaining a <command>systemd</command>-less system. <ulink type=\"block\" url=\"https://devuan.org\" />"
msgstr ""

msgid "DoudouLinux"
msgstr "DoudouLinux"

msgid "<primary>DoudouLinux</primary>"
msgstr "<primary>DoudouLinux</primary>"

msgid "DoudouLinux targets young children (starting from 2 years old). To achieve this goal, it provides a heavily customized graphical interface (based on LXDE) and comes with many games and educative applications. Internet access is filtered to prevent children from visiting problematic websites. Advertisements are blocked. The goal is that parents should be free to let their children use their computer once booted into DoudouLinux. And children should love using DoudouLinux, just like they enjoy their gaming console. <ulink type=\"block\" url=\"https://www.doudoulinux.org\" />"
msgstr ""

msgid "Raspbian"
msgstr "Raspbian"

msgid "<primary>Raspbian</primary>"
msgstr "<primary>Raspbian</primary>"

msgid "<primary>Raspberry Pi</primary>"
msgstr "<primary>Raspberry Pi</primary>"

msgid "Raspbian is a rebuild of Debian optimized for the popular (and inexpensive) Raspberry Pi family of single-board computers. The hardware for that platform is more powerful than what the Debian <emphasis role=\"architecture\">armel</emphasis> architecture can take advantage of, but lacks some features that would be required for <emphasis role=\"architecture\">armhf</emphasis>; so Raspbian is a kind of intermediary, rebuilt specifically for that hardware and including patches targeting this computer only. <ulink type=\"block\" url=\"https://raspbian.org\" />"
msgstr ""

msgid "PureOS"
msgstr ""

#, fuzzy
#| msgid "<primary>Grml</primary>"
msgid "<primary>PureOS</primary>"
msgstr "<primary>Grml</primary>"

#, fuzzy
#| msgid "<primary>Grml</primary>"
msgid "<primary>Purism</primary>"
msgstr "<primary>Grml</primary>"

msgid "PureOS is a Debian-based distribution focused on privacy, convenience and security. It follows the <ulink url=\"https://www.gnu.org/distros/free-system-distribution-guidelines.html\">GNU Free System Distribution Guidelines</ulink>, used by the Free Software Foundation to qualify a distribution as free. The social purpose company Purism guides its development. <ulink type=\"block\" url=\"https://pureos.net/\" />"
msgstr ""

msgid "SteamOS"
msgstr ""

#, fuzzy
#| msgid "<primary>Devuan</primary>"
msgid "<primary>SteamOS</primary>"
msgstr "<primary>Devuan</primary>"

#, fuzzy
#| msgid "<primary>Kali</primary>"
msgid "<primary>Valve Corporation</primary>"
msgstr "<primary>Kali</primary>"

msgid "SteamOS is a gaming-oriented Debian-based distribution developed by Valve Corporation. It is used in the Steam Machine, a line of gaming computers. <ulink type=\"block\" url=\"https://store.steampowered.com/steamos/\" />"
msgstr ""

msgid "And Many More"
msgstr "還有更多"

msgid "<primary>Distrowatch</primary>"
msgstr "<primary>Distrowatch</primary>"

msgid "The Distrowatch website references a huge number of Linux distributions, many of which are based on Debian. Browsing this site is a great way to get a sense of the diversity in the free software world. <ulink type=\"block\" url=\"https://distrowatch.com\" />"
msgstr ""

msgid "The search form can help track down a distribution based on its ancestry. In June 2019, selecting Debian led to 127 active distributions! <ulink type=\"block\" url=\"https://distrowatch.com/search.php\" />"
msgstr ""

#~ msgid "Tanglu"
#~ msgstr "Tanglu"

#~ msgid "<primary>Tanglu</primary>"
#~ msgstr "<primary>Tanglu</primary>"
